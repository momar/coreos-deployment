# CoreOS Deployment

This repository contains my [Fedora CoreOS](https://getfedora.org/coreos/) deployment setup for [Hetzner](https://hetzner.de/).
Basically, it's based on the following ideas & tools:

- [Terraform](https://terraform.io/) is used to deploy the server on Hetzner & install CoreOS.
- [CoreOS Ignition](https://docs.fedoraproject.org/en-US/fedora-coreos/fcct-config/) is used to perform all required configuration changes on the server.
- [Docker](https://docker.com/) and [Docker Compose](https://docs.docker.com/compose/) is used to configure & separate the projects.
- [Git](https://git-scm.com/) is used to push updates to the projects configuration.
- [Borg](https://borgbackup.readthedocs.io/) is used for daily backups with automatic restores on server creation.

During deployment (with `terraform apply`), it requires [Docker](https://docker.com/) and [yaml-merge](https://github.com/alexlafroscia/yaml-merge) for FCCT on the executing system.

## Open Tasks
- [ ] IPv6 support (postponed due to bad support by Docker & missing autoconfig at Hetzner)
- [ ] Automatically set IPs in my DNS at hosting.de (postponed due to missing hosting.de support in Terraform)
- [ ] Add an example /var/mnt/projects Git repo for a quick start (Traefik, Diun, Ofelia, acme.sh)
- [ ] Create a Webhook server based on Docker labels
- [ ] Documentation & blog articles
